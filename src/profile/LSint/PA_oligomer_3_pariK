#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > PA_oligomer_3_pariK.info <<'%EOF%'
   PA_oligomer_3_pariK
   -------------
   Molecule:         C60/6-31G
   Wave Function:    HF
   Profile:          Exchange Matrix
   CPU Time:         9 min 30 seconds
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > PA_oligomer_3_pariK.mol <<'%EOF%'
BASIS
cc-pVTZ Aux=cc-pVTZdenfit
EXCITATION DIAGNOSTIC
CAM-B3LYP/6-31G* GEOMETRY
Atomtypes=2  Nosymmetry
Charge=6.0 Atoms=4
C2       -0.7491155004            1.1578446288            0.0000000000
C2        0.7491155004           -1.1578446288            0.0000000000
C4        3.2671719600           -1.2092631861            0.0000000000
C4       -3.2671719600            1.2092631861            0.0000000000
Charge=1.0 Atoms=6
H2        0.3066776431            2.9245553717            0.0000000000
H2       -0.3066776431           -2.9245553717            0.0000000000
H4        4.3743429551            0.5208084576            0.0000000000
H4       -4.3743429551           -0.5208084576            0.0000000000
H6        4.3129268723           -2.9724279517            0.0000000000
H6       -4.3129268723            2.9724279517            0.0000000000

%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > PA_oligomer_3_pariK.dal <<'%EOF%'
**PROFILE
.EXCHANGE
**INTEGRALS
.PARI-K
**WAVE FUNCTIONS
.HF
*DENSOPT
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >PA_oligomer_3_pariK.check
cat >> PA_oligomer_3_pariK.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "Exchange energy, mat_dotproduct\(D,K\)\= * \-XXX\.XXXXXX" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="ENERGY NOT CORRECT -"

PASSED=1
for i in 1
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo PROF ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
