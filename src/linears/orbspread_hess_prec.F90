module orbspread_hess_prec_mod
!##########################################################
!#            GENERAL INTERFACE ROUTINES                  #
!# Below are routine that are kept outside modules.       #
!# They are interface routines to solvers (precond/       #
!# linear trans.) and lsdalton main program (optimloc).   #
!#                                                        #
!##########################################################
  use precision
  use matrix_module, only: matrix
  use matrix_operations 
  use matrix_operations_aux 
  use matrix_operations_scalapack
  use matrix_util!, only: matrix_exponential
  use matrix_util
  use loc_utils
  use typedeftype
  use LSTIMING
  use localitymeasureMod
  use decompMod
#ifdef VAR_ENABLE_TENSORS
  use reorder_frontend_module, only: array_reorder
#endif
  private
  public :: orbspread_hesslin, orbspread_precond, &
       & free_mem_lintran, alloc_mem_lintran

  real(realk),pointer :: tmp1(:),tmp2(:),tmp3(:),tmp4(:)
  real(realk),pointer :: tmp5(:),tmp6(:),tmp7(:),tmp8(:)
  real(realk),pointer :: orbhesstmpMx(:,:),orbhesstmpMy(:,:),orbhesstmpMz(:,:)  
  real(realk),pointer :: orbhessQV(:,:)

CONTAINS
  subroutine alloc_mem_lintran(norb)
    implicit none
    integer,intent(in) :: norb

    IF(matrix_type.EQ.mtype_dense)THEN
       call mem_alloc(tmp1,norb)
       call mem_alloc(tmp2,norb)
       call mem_alloc(tmp3,norb)
       call mem_alloc(tmp4,norb)
       call mem_alloc(tmp5,norb)
       call mem_alloc(tmp6,norb)
       call mem_alloc(tmp7,norb)
       call mem_alloc(tmp8,norb)
       call mem_alloc(orbhesstmpMx,norb,norb)
       call mem_alloc(orbhesstmpMy,norb,norb)
       call mem_alloc(orbhesstmpMz,norb,norb)
       call mem_alloc(orbhessQV,norb,norb)
    ENDIF

  end  subroutine alloc_mem_lintran

  subroutine free_mem_lintran()
    implicit none

    IF(matrix_type.EQ.mtype_dense)THEN
       call mem_dealloc(tmp1)
       call mem_dealloc(tmp2)
       call mem_dealloc(tmp3)
       call mem_dealloc(tmp4)
       call mem_dealloc(tmp5)
       call mem_dealloc(tmp6)
       call mem_dealloc(tmp7)
       call mem_dealloc(tmp8)
       call mem_dealloc(orbhesstmpMx)
       call mem_dealloc(orbhesstmpMy)
       call mem_dealloc(orbhesstmpMz)
       call mem_dealloc(orbhessQV)
    ENDIF
    
  end subroutine free_mem_lintran
    
!> \brief hessian linear transformation for power m orbital spread locality measure
!> \author B. Jansik (modified by Thomas Kjaergaard)
!> \date 2010
!> \param Hv linear transformation of H with trial vector V
!> \param V, trial vector
!> \param mu, level shift (H-muI)
!> \param norb, size of matrices (same as number of orbitals involved in localization)
!> \param orbspread_input structure holding orbspread related data
!!> \param m power
!!> \param spread2 vector of squared orbital spreads 
!!> \param R(3) matrix array holding XDIPLEN,YDIPLEN,ZDIPLEN components
!!> \param Q matrix holding sum XXSECMOM+YYSECMOM+ZZSECMOM
!!> \param tmpM(4) matrix array used as workspace
!> all parameters must be allocated prior to subroutine call
!> all type(matrix) arguments are of norb,norb size
    subroutine orbspread_hesslin(Hv,V,mu,norb,orbspread_input)!m,spread2,R,Q,tmpM)
      implicit none

      Type(Matrix), intent(inout) :: Hv
      Type(Matrix), intent(in)  :: V
      real(realk), intent(in)   :: mu
      integer, intent(in)       :: norb
      !  type(orbspread_data), intent(in), target :: orbspread_input
      type(orbspread_data), target :: orbspread_input

      integer       :: m
      !  Type(Matrix)  :: R(3), RV(3)
      Type(Matrix), pointer  ::  Q, G
      Type(Matrix) ::  QV ,tmpM,hv2,hv3,hv4,hv7,hv9
      real(realk), pointer   :: spread2(:)
      real(realk)  :: TS,TE

      Q       => orbspread_input%Q
      G       => orbspread_input%G
      spread2 => orbspread_input%spread2
      m       =  orbspread_input%m
#if VAR_LSDEBUG
      IF(Hv%nrow.NE.norb)call lsquit('HV%nrow',-1)
      IF(Hv%ncol.NE.norb)call lsquit('HV%ncol',-1)
      IF(V%nrow.NE.norb)call lsquit('V%nrow',-1)
      IF(V%ncol.NE.norb)call lsquit('V%ncol',-1)
      IF(Q%nrow.NE.norb)call lsquit('Q%nrow',-1)
      IF(Q%ncol.NE.norb)call lsquit('Q%ncol',-1)
      IF(G%nrow.NE.norb)call lsquit('G%nrow',-1)
      IF(G%ncol.NE.norb)call lsquit('G%ncol',-1)
#endif
      
      select case(matrix_type)             
      case(mtype_dense)
         call orbspread_hesslin_full(Hv%elms,V%elms,mu,norb,orbspread_input,&
              & Q%elms,G%elms,spread2,m,orbspread_input%R(1)%elms,&
              & orbspread_input%R(2)%elms,orbspread_input%R(3)%elms)
      case(mtype_scalapack)         
         call orbspread_hesslin_scalapack(Hv,V,mu,norb,orbspread_input,&
              & Q,G,spread2,m,orbspread_input%R(1),&
              & orbspread_input%R(2),orbspread_input%R(3))                
      case default
         call orbspread_hesslin_default(Hv,V,mu,norb,orbspread_input,Q,G,m,spread2)
      end select
    end subroutine orbspread_hesslin

    !> \brief hessian linear transformation for power m orbital spread locality measure
    !> \author Thomas Kjaergaard
    !> \date 2017
    !> \param Hv linear transformation of H with trial vector V
    !> \param V, trial vector
    !> \param mu, level shift (H-muI)
    !> \param norb, size of matrices (same as number of orbitals involved in localization)
    !> \param orbspread_input structure holding orbspread related data
    !!> \param m power
    !!> \param spread2 vector of squared orbital spreads 
    !!> \param R(3) matrix array holding XDIPLEN,YDIPLEN,ZDIPLEN components
    !!> \param Q matrix holding sum XXSECMOM+YYSECMOM+ZZSECMOM
    !> all parameters must be allocated prior to subroutine call
    !> all type(matrix) arguments are of norb,norb size
    subroutine orbspread_hesslin_default(Hv,V,mu,norb,orbspread_input,Q,G,m,spread2)
      implicit none
      Type(Matrix), intent(inout) :: Hv
      Type(Matrix), intent(in)  :: V,G,Q
      real(realk), intent(in)   :: mu
      integer, intent(in)       :: norb,m
      !  type(orbspread_data), intent(in), target :: orbspread_input
      real(realk),intent(in)  :: spread2(norb)
      type(orbspread_data), target :: orbspread_input
      !
      Type(Matrix) ::  QV ,tmpM,hv2,hv3,hv4,hv7,hv9
      real(realk)  :: diagQV(norb),diagR(norb,3),diagRV(norb,3),tmp(norb),TS,TE
      Type(Matrix) :: inptmpMx
      integer      :: x,y,i

      call mat_zero(HV)
      !job
      call mat_init(QV,Q%nrow,V%ncol)
      call mat_mul(Q,V,'n','n',1E0_realk,0E0_realk,QV)
      call mat_extract_diagonal(diagQV,QV)
      do i=1,norb
         tmp(i) = diagQV(i)*(spread2(i)**(m-2))
      enddo
      call mat_zero(Hv)
      call mat_dmul(tmp,Q,'n',-4E0_realk*m*(m-1),0E0_realk,Hv)
      do i=1,norb
         tmp(i) =  (spread2(i)**(m-1))
      enddo
      call mat_dmul(tmp,QV,'t',-2E0_realk*m,1E0_realk,Hv)
      call mat_dmul(tmp,QV,'n',-2E0_realk*m,1E0_realk,Hv)
      call mat_free(QV)
      
      call mat_init(inptmpMx,orbspread_input%R(1)%nrow,V%ncol)
      do x=1, 3
         call mat_mul(orbspread_input%R(x),V,'n','n',1E0_realk,0E0_realk,inptmpMx)
         call mat_extract_diagonal(diagRV(:,x),inptmpMx)
         !FIXME PREEXTRACT diagR(:,x) from orbspread_input%R(x) orbspread_input%diagR(x)%p 
         call mat_extract_diagonal(diagR(:,x),orbspread_input%R(x))
      enddo
      do x=1, 3
         call mat_mul(orbspread_input%R(x),V,'n','n',1E0_realk,0E0_realk,inptmpMx)
         do i=1,norb
            tmp(i) = diagR(i,x)*diagQV(i)*(spread2(i)**(m-2))
         enddo
         call mat_dmul(tmp,orbspread_input%R(x),'n',8E0_realk*m*(m-1),1E0_realk,Hv)
         
         do i=1,norb
            tmp(i) = diagR(i,x)*diagRV(i,x)*(spread2(i)**(m-2))
         enddo
         call mat_dmul(tmp,Q,'n',8E0_realk*m*(m-1),1E0_realk,Hv)
         
         do y=1, 3
            do i=1,norb
               tmp(i) = diagR(i,x)*diagR(i,y)*diagRV(i,x)*(spread2(i)**(m-2))
            enddo
            call mat_dmul(tmp,orbspread_input%R(y),'n',-16E0_realk*m*(m-1),1E0_realk,Hv)
         enddo
         
         do i=1,norb
            tmp(i) = diagRV(i,x)*(spread2(i)**(m-1))
         enddo
         call mat_dmul(tmp,orbspread_input%R(x),'n',8E0_realk*m,1E0_realk,Hv)
         
         do i=1,norb
            tmp(i) = diagR(i,x)*(spread2(i)**(m-1))
         enddo
         call mat_dmul(tmp,inptmpMx,'t',4E0_realk*m,1E0_realk,Hv)
         
         call mat_dmul(tmp,inptmpMx,'n',4E0_realk*m,1E0_realk,Hv)
      enddo
      call mat_free(inptmpMx)
      
      call mat_mul(V,G,'n','n',0.5E0_realk,1E0_realk,Hv)
      !call mat_mul(V,G,'n','n',1E0_realk,1E0_realk,Hv)
      
      call mat_init(tmpM,Hv%ncol,Hv%nrow)
      call mat_trans(Hv,tmpM)
      call mat_daxpy(-1E0_realk,tmpM,Hv)
      call mat_free(tmpM)
      
      !call mat_scal(0.5E0_realk,Hv)
      if (dabs(mu) > 1.0E-8_realk) call mat_daxpy(-mu,V,Hv)

    end subroutine orbspread_hesslin_default

    !> \brief SCALAPACK version hessian linear transformation for power m orbital spread locality measure
    !> \author Thomas Kjaergaard
    !> \date 2017
    !> \param Hv linear transformation of H with trial vector V
    !> \param V, trial vector
    !> \param mu, level shift (H-muI)
    !> \param norb, size of matrices (same as number of orbitals involved in localization)
    !> \param orbspread_input structure holding orbspread related data
    !!> \param m power
    !!> \param spread2 vector of squared orbital spreads 
    !!> \param R(3) matrix array holding XDIPLEN,YDIPLEN,ZDIPLEN components
    !!> \param Q matrix holding sum XXSECMOM+YYSECMOM+ZZSECMOM
    !> all parameters must be allocated prior to subroutine call
    !> all type(matrix) arguments are of norb,norb size
    subroutine orbspread_hesslin_scalapack(Hv,V,mu,norb,orbspread_input,&
         & Q,G,spread2,m,Rx,Ry,Rz)
      implicit none
      Type(Matrix), intent(inout) :: Hv
      Type(Matrix), intent(in)  :: V,Q,G,Rx,Ry,Rz
      real(realk), intent(in)   :: mu
      integer, intent(in)       :: norb,m
      !  type(orbspread_data), intent(in), target :: orbspread_input
      type(orbspread_data), target :: orbspread_input
      Type(Matrix)  ::  QV,inptmpMx,inptmpMy,inptmpMz           
      real(realk)  :: spread2(:)
      integer      :: x,y,i
      
      call mat_zero(HV)
      !job
      call mat_init(QV,Q%nrow,V%ncol)
      call mat_init(inptmpMx,norb,norb)
      call mat_init(inptmpMy,norb,norb)
      call mat_init(inptmpMz,norb,norb)
      
      call mat_mul(Q,V,'n','n',1E0_realk,0E0_realk,QV)
      call mat_mul(Rx,V,'n','n',1E0_realk,0E0_realk,inptmpMx)
      call mat_mul(Ry,V,'n','n',1E0_realk,0E0_realk,inptmpMy)
      call mat_mul(Rz,V,'n','n',1E0_realk,0E0_realk,inptmpMz)
      
      call mat_scalapack_orbspread_hesslin(HV,Rx,Ry,Rz,Q,QV,inptmpMx,inptmpMy,inptmpMz,&
           & orbspread_input%diagR(1)%p,orbspread_input%diagR(2)%p,&
           & orbspread_input%diagR(3)%p,m,spread2,norb)
      
      call mat_mul(V,G,'n','n',0.5E0_realk,1E0_realk,Hv)      
      call mat_trans(Hv,inptmpMx)
      call mat_daxpy(-1E0_realk,inptmpMx,Hv)
      
      !call mat_scal(0.5E0_realk,Hv)
      if (dabs(mu) > 1.0E-8_realk) call mat_daxpy(-mu,V,Hv)
      
      call mat_free(QV)
      call mat_free(inptmpMx)
      call mat_free(inptmpMy)
      call mat_free(inptmpMz)
      
    end subroutine orbspread_hesslin_scalapack
    
    subroutine orbspread_transpose(alpha,Hv,dims,order,beta,inptmpMx)
      implicit none
      integer :: dims(2),order(2)
      real(realk) :: alpha,beta
#ifdef VAR_ENABLE_TENSORS
      real(realk) :: Hv(dims(1)*dims(2)),inptmpMx(dims(1)*dims(2))
      call array_reorder(alpha,Hv,dims,order,beta,inptmpMx)
#else
      real(realk) :: Hv(dims(1),dims(2)),inptmpMx(dims(2),dims(1))
      integer :: i,j
      !$OMP PARALLEL DO COLLAPSE(2) DEFAULT(SHARED) PRIVATE(I,J)
      do j = 1,dims(1)
         do i = 1,dims(2)
            inptmpMx(j,i) = Hv(i,j)
         enddo
      enddo
      !$OMP END PARALLEL DO
#endif
    end subroutine orbspread_transpose
    
    !> \brief hessian linear transformation for power m orbital spread locality measure
    !> \author B. Jansik
    !> \date 2010
    !> \param Hv linear transformation of H with trial vector V
    !> \param V, trial vector
    !> \param mu, level shift (H-muI)
    !> \param norb, size of matrices (same as number of orbitals involved in localization)
    !> \param orbspread_input structure holding orbspread related data
    !> all parameters must be allocated prior to subroutine call
    !> all type(matrix) arguments are of norb,norb size
    subroutine orbspread_hesslin_full(Hv,V,mu,norb,orbspread_input,Q,G,spread2,m,Rx,Ry,Rz)
      implicit none
      integer, intent(in)       :: norb
      real(realk), intent(inout) :: Hv(norb,norb)
      real(realk), intent(in)  :: V(norb,norb)
      real(realk), intent(in)   :: mu
      !  type(orbspread_data), intent(in), target :: orbspread_input
      type(orbspread_data), target :: orbspread_input
      real(realk) ::  Q(norb,norb), G(norb,norb)
      real(realk) ::  Rx(norb,norb), Ry(norb,norb), Rz(norb,norb)
      real(realk)  :: spread2(norb)
      integer       :: m
      !
!      real(realk),pointer :: tmpM(:,:)
      real(realk)  :: diagQV(norb),diagRV(norb,3),TS,TE
      integer      :: x,y,i,j,dims(2),order(2)
      !FULL i,j for MPI/OpenMP
!      call ls_dzero(Hv,norb*norb)
      

      !4 independent DGEMMS

#ifdef VAR_OMP      
      !$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(I,J) FIRSTPRIVATE(norb)
      do I=1,4
         IF(I.EQ.1)THEN
            call DGEMM('n','n',norb,norb,norb,1E0_realk,Rx,norb,V,norb,0E0_realk,orbhesstmpMx,norb)
            do j=1,norb
               diagRV(j,1) = orbhesstmpMx(j,j)
            enddo
         ELSEIF(I.EQ.2)THEN
            call DGEMM('n','n',norb,norb,norb,1E0_realk,Ry,norb,V,norb,0E0_realk,orbhesstmpMy,norb)
            do j=1,norb
               diagRV(j,2) = orbhesstmpMy(j,j)
            enddo
         ELSEIF(I.EQ.3)THEN
            call DGEMM('n','n',norb,norb,norb,1E0_realk,Rz,norb,V,norb,0E0_realk,orbhesstmpMz,norb)
            do j=1,norb
               diagRV(j,3) = orbhesstmpMz(j,j)
            enddo
         ELSE
            call DGEMM('n','n',norb,norb,norb,1E0_realk,Q,norb,V,norb,0E0_realk,orbhessQV,norb)
            do j=1,norb
               diagQV(j) = orbhessQV(j,j)
            enddo
         ENDIF
      enddo
      !$OMP END PARALLEL DO
#else
      call DGEMM('n','n',norb,norb,norb,1E0_realk,Rx,norb,V,norb,0E0_realk,orbhesstmpMx,norb)
      call DGEMM('n','n',norb,norb,norb,1E0_realk,Ry,norb,V,norb,0E0_realk,orbhesstmpMy,norb)
      call DGEMM('n','n',norb,norb,norb,1E0_realk,Rz,norb,V,norb,0E0_realk,orbhesstmpMz,norb)
      call DGEMM('n','n',norb,norb,norb,1E0_realk,Q,norb,V,norb,0E0_realk,orbhessQV,norb)
      do i=1,norb
         diagRV(i,1) = orbhesstmpMx(i,i)
         diagRV(i,2) = orbhesstmpMy(i,i)
         diagRV(i,3) = orbhesstmpMz(i,i)
         diagQV(i) = orbhessQV(i,i)
      enddo
#endif
      
      
      !$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(I)
      do i=1,norb
         tmp1(i) = 8E0_realk*m*(m-1)*orbspread_input%diagR(1)%p(i)*diagQV(i)*(spread2(i)**(m-2))&
              &  -16E0_realk*m*(m-1)*orbspread_input%diagR(1)%p(i)*orbspread_input%diagR(1)%p(i)*&
              & diagRV(i,1)*(spread2(i)**(m-2))&
              &  -16E0_realk*m*(m-1)*orbspread_input%diagR(2)%p(i)*orbspread_input%diagR(1)%p(i)*&
              & diagRV(i,2)*(spread2(i)**(m-2))&
              &  -16E0_realk*m*(m-1)*orbspread_input%diagR(3)%p(i)*orbspread_input%diagR(1)%p(i)*&
              & diagRV(i,3)*(spread2(i)**(m-2))&         
              & + 8E0_realk*m*diagRV(i,1)*(spread2(i)**(m-1))

         tmp2(i) = 8E0_realk*m*(m-1)*orbspread_input%diagR(2)%p(i)*diagQV(i)*(spread2(i)**(m-2))&
              &  -16E0_realk*m*(m-1)*orbspread_input%diagR(1)%p(i)*orbspread_input%diagR(2)%p(i)*&
              & diagRV(i,1)*(spread2(i)**(m-2))&
              &  -16E0_realk*m*(m-1)*orbspread_input%diagR(2)%p(i)*orbspread_input%diagR(2)%p(i)*&
              & diagRV(i,2)*(spread2(i)**(m-2))&
              &  -16E0_realk*m*(m-1)*orbspread_input%diagR(3)%p(i)*orbspread_input%diagR(2)%p(i)*&
              & diagRV(i,3)*(spread2(i)**(m-2))&
              & + 8E0_realk*m*diagRV(i,2)*(spread2(i)**(m-1))

         tmp3(i) = 8E0_realk*m*(m-1)*orbspread_input%diagR(3)%p(i)*diagQV(i)*(spread2(i)**(m-2))&
              &  -16E0_realk*m*(m-1)*orbspread_input%diagR(1)%p(i)*orbspread_input%diagR(3)%p(i)*&
              & diagRV(i,1)*(spread2(i)**(m-2))&
              &  -16E0_realk*m*(m-1)*orbspread_input%diagR(2)%p(i)*orbspread_input%diagR(3)%p(i)*&
              & diagRV(i,2)*(spread2(i)**(m-2))&
              &  -16E0_realk*m*(m-1)*orbspread_input%diagR(3)%p(i)*orbspread_input%diagR(3)%p(i)*&
              & diagRV(i,3)*(spread2(i)**(m-2))&
              & + 8E0_realk*m*diagRV(i,3)*(spread2(i)**(m-1))

         tmp4(i) = 4E0_realk*m*orbspread_input%diagR(1)%p(i)*(spread2(i)**(m-1))
         tmp5(i) = 4E0_realk*m*orbspread_input%diagR(2)%p(i)*(spread2(i)**(m-1))
         tmp6(i) = 4E0_realk*m*orbspread_input%diagR(3)%p(i)*(spread2(i)**(m-1))

         tmp7(i) = - 4E0_realk*m*(m-1)*diagQV(i)*(spread2(i)**(m-2)) &
              &    + 8E0_realk*m*(m-1)*orbspread_input%diagR(1)%p(i)*diagRV(i,1)*(spread2(i)**(m-2))&
              &    + 8E0_realk*m*(m-1)*orbspread_input%diagR(2)%p(i)*diagRV(i,2)*(spread2(i)**(m-2))&
              &    + 8E0_realk*m*(m-1)*orbspread_input%diagR(3)%p(i)*diagRV(i,3)*(spread2(i)**(m-2))
         tmp8(i) = -2E0_realk*m*(spread2(i)**(m-1))
      enddo
      !$OMP END PARALLEL DO

      !$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(I,J)
      do i=1,norb
         do j=1,norb
            Hv(j,i) = tmp1(j)*Rx(j,i) + tmp2(j)*Ry(j,i) + tmp3(j)*Rz(j,i) &
                 &  + tmp4(j)*orbhesstmpMx(j,i)+ tmp5(j)*orbhesstmpMy(j,i)+ tmp6(j)*orbhesstmpMz(j,i) &
                 &  + tmp4(j)*orbhesstmpMx(i,j)+ tmp5(j)*orbhesstmpMy(i,j)+ tmp6(j)*orbhesstmpMz(i,j) &
                 &  + tmp7(j)*Q(j,i)+ tmp8(j)*orbhessQV(j,i)+ tmp8(j)*orbhessQV(i,j)
         enddo
      enddo
      !$OMP END PARALLEL DO
            
      !call mat_mul(V,G,'n','n',0.5E0_realk,0E0_realk,Hv)
      call DGEMM('n','n',norb,norb,norb,0.5E0_realk,V,norb,G,norb,1E0_realk,Hv,norb)
      order(1)=2
      order(2)=1
      dims(1)=norb
      dims(2)=norb
      call orbspread_transpose(1.0E0_realk,Hv,dims,order,0.0E0_realk,orbhesstmpMx)
      call daxpy(norb*norb,-1.0E0_realk,orbhesstmpMx,1,Hv,1)


      !call mat_scal(0.5E0_realk,Hv)
      if (dabs(mu) > 1.0E-8_realk) call daxpy(norb*norb,-mu,V,1,Hv,1)
      
    end subroutine orbspread_hesslin_full

    subroutine orbspread_precond(Xout,X,mu,inp)
      implicit none
      type(Matrix), intent(inout) :: Xout
      type(Matrix) :: X
      real(realk),  intent(in) :: mu
      type(orbspread_data), intent(in) :: inp
      real(realk),pointer   :: tmp(:), tmpP(:)
      integer                   :: i, ne
      real(realk),pointer :: xvec(:),yvec(:)
      type(matrix)  :: xtemp

      ne = Xout%nrow*Xout%ncol

      call mat_zero(Xout)

      select case(matrix_type)
      case(mtype_dense)
         !$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(I)
         do i=1,ne
            if (dabs(inp%P%elms(i)- mu) > 1d-8) Xout%elms(i) = X%elms(i)/(inp%P%elms(i) - mu)
         enddo
         !$OMP END PARALLEL DO
      case(mtype_scalapack)
         call mat_copy(1d0,X,Xout)
         call mat_hdiv(Xout,inp%P,mu)
      case default

         call mem_alloc(tmp,X%nrow*X%ncol)
         call mat_to_full(X,1E0_realk,tmp)
         call mem_alloc(tmpP,inp%P%nrow*inp%P%ncol)
         call mat_to_full(inp%P,1E0_realk,tmpP)

         !$OMP PARALLEL DO DEFAULT(SHARED) PRIVATE(I)
         do i=1,ne
            if (dabs(tmpP(i) - mu)> 1d-8) tmp(i) = tmp(i)/(tmpP(i) - mu)
         enddo
         !$OMP END PARALLEL DO
 
         call mem_dealloc(tmpP)
         
         call mat_set_from_full(tmp,1E0_realk,Xout)

         call mem_dealloc(tmp)

      end select

    end subroutine orbspread_precond

end module orbspread_hess_prec_mod
