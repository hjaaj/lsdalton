!> @file
!> Contains module for MPI integral evaluation settings
module infpar_module
use precision
#ifdef VAR_MPI

type infpar_struct
  integer :: nodeid(129), ncode,  iprpar, mtottk, ntask,&
       &nfmat,  ndegdi, mytid,  timing,   slave,&
       &debug
  integer :: inputBLOCKSIZE
  character(20) :: nodnam(128), myname
  !> Master rank
  integer(kind=ls_mpik) :: master
  !> Rank inside total (world) group
  integer(kind=ls_mpik) :: mynum
  !> Total number of nodes inside total (world) group
  integer(kind=ls_mpik) :: nodtot
  !> Rank inside local group where one of the "slaves" is a local master
  integer(kind=ls_mpik) :: lg_mynum
  !> Total number of nodes local group where one of the "slaves" is a local master
  integer(kind=ls_mpik) :: lg_nodtot
  !> Communicator for local group where one of the "slaves" is a local master
  integer(kind=ls_mpik) :: lg_comm
  !> number of nodes used in scalapack
  integer(kind=ls_mpik) :: ScalapackGroupSize
  !> Enforce fully memory distrubuted run
  logical :: ScalapackFORCEMEMDIST
  !> Enforce fully memory distrubuted run
  integer :: ScalapackFORCEMEMDISTnbast
  !> Use MPI IO 
  logical :: ScalapackMPIIO

  !> Are there more local jobs?
  logical :: lg_morejobs
  logical :: mpimemdebug
  integer(kind=8) :: pagesize
  integer(kind=8) :: hugepagesize
end type infpar_struct

type(infpar_struct) :: infpar
!> specifies if mpi_init should be called in lsmpi_init
logical, save :: call_mpi_init = .true.
private
public :: call_mpi_init,infpar,infpar_struct, init_infpar

#endif
contains

subroutine init_infpar()
#ifdef VAR_MPI
  infpar%pagesize = 4096 !basic page size in bytes (default is 4K)
  infpar%hugepagesize = 2097152 !huge page size in bytes (defaults to 2M)
  infpar%mpimemdebug = .FALSE.
#endif
end subroutine init_infpar

end module infpar_module
