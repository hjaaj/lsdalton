Each directory in this directory contains a set of molecule input files
that can be used for benchmarking on new methods. 

Feel free to add a directory with a benchmark set.

Each benchmark directory should include

1. a reference to where this benchmark set was first presented
2. possible a list of references where this benchmark set have been used
   so it the result can easily be compared to other similar methods
3. list of atoms this benchmark include for instance only (H-Ar) or (H-Kr) ..
4. sizes of the molecules involved
5. closed shell molecules only or openshell molecules only or mixed 
6. properties it has been benchmarked for (reference)
7. maybe which basis set have been used before (and we therefore know they work)
  
