MODULE ls_inputAO_mod
use precision
use lsparameters
use ao_typetype, only: aoitem
use ao_type, only: free_aoitem
use basis_typetype, only: BASIS_PT,BASISINFO,BASISSETINFO,nBasisBasParam,RegBasParam,AUXBasParam, &
      &                   CABBasParam,JKBasParam,VALBasParam,GCTBasParam,ADMBasParam

use molecule_typetype, only: MOLECULE_PT,MOLECULEINFO
use typeDefType, only: lssetting,lsintscheme
use typeDef, only: getNbasis
use buildaobatch, only: build_ao,build_empty_ao,build_empty_elfield_ao,build_empty_nuclear_ao, &
      &                 build_empty_pcharge_ao,build_empty_single_nuclear_ao,build_shellbatch_ao, &
      &                 determinenaobatches


public :: BuildInputAO,setAObatch,freeInputAO

PRIVATE

CONTAINS
!> \brief Builds the input AOs
!> \author T. Kjaergaard and S. Reine
!> \date 2016
!> \param AO1 the type of the basis ('Regular','DF-Aux',AOEmpty) on center 1
!> \param AO2 the type of the basis ('Regular','DF-Aux',AOEmpty) on center 2
!> \param AO3 the type of the basis ('Regular','DF-Aux',AOEmpty) on center 3
!> \param AO4 the type of the basis ('Regular','DF-Aux',AOEmpty) on center 4
!> \param intType the label for primitive or contracted calc
!> \param AObuild the list of AOITEMS to be build
!> \param nAObuilds the number of AOITEMS build
!> \param indAO connects AO index 1-4 with the correct AO build
!> \param ndim the dimension of each AO
!> \param LHSGAB if this is a LHS screening matrix
!> \param sameLHSaos the AO1 and 2 are the same (i.e. same fragment, type and basis)
!> \param sameRHSaos the AO3 and 4 are the same (i.e. same fragment, type and basis)
!> \param sameODs the AO1-2 products are the same as the AO3-4 products
!> \param SETTING Integral evalualtion settings
!> \param LUPRI logical unit number of the Default output file
!> \param LUERR logical unit number of the Default error file
SUBROUTINE BuildInputAO(AO1,AO2,AO3,AO4,intType,AObuild,nAObuilds,indAO,ndim,CSint,LHSGAB,&
     &                  sameLHSaos,sameRHSaos,sameODs,SETTING,LUPRI,LUERR)
implicit none
integer,intent(IN)         :: AO1,AO2,AO3,AO4,intType,LUPRI,LUERR
Type(LSSETTING),intent(in) :: SETTING
Integer,intent(OUT)        :: nAObuilds,indAO(4),ndim(4)
TYPE(AOITEM),target        :: AObuild(4)
LOGICAL,intent(IN)         :: LHSGAB !THIS ONLY HAS AN EFFECT WHEN USING FRAGMENTS 
LOGICAL,intent(IN)         :: CSint
LOGICAL,intent(OUT)        :: sameLHSaos,sameRHSaos,sameODs
!
TYPE(MOLECULE_PT),pointer  :: FRAGMENTS(:)
Integer                    :: IAO,JAO,ind
integer                    :: AOstring(4)
TYPE(BASIS_PT)             :: AObasis(4)
Logical                    :: uniqueAO,emptyAO,intnrm,sameFrag(4,4)
Integer                    :: indexUnique(4),AObatchdim,batchindex(4),batchsize(4)
IF (setting%nAO.NE. 4) CALL LSQUIT('Error in BuildInputAO. nAO .NE. 4',lupri)
NULLIFY(FRAGMENTS)
ALLOCATE(FRAGMENTS(4))

!Used for testing if the AOs are identical (depends also in the AO-strings)
sameFrag = setting%sameFrag
batchindex = SETTING%batchindex
batchsize = SETTING%batchsize

AOstring(1) = AO1
AOstring(2) = AO2
AOstring(3) = AO3
AOstring(4) = AO4

DO iAO=1,setting%nAO
  FRAGMENTS(iAO)%p => SETTING%FRAGMENT(iAO)%p
ENDDO
DO iAO=1,setting%nAO
   AObasis(iAO)%p => Setting%BASIS(iAO)%p
ENDDO
sameLHSaos = (AO1.EQ.AO2) .AND. (.NOT. AO1.EQ.AOEmpty).AND.samefrag(1,2)
sameRHSaos = (AO3.EQ.AO4) .AND. (.NOT. AO3.EQ.AOEmpty).AND.samefrag(3,4)
sameODs    = (AO1.EQ.AO3) .AND. (AO2.EQ.AO4).AND.samefrag(1,3).AND.samefrag(2,4)
!Specical settings for Cauchy-Schwarz screening integrals
IF(CSint)THEN
   IF(LHSGAB)THEN
      FRAGMENTS(3)%p => FRAGMENTS(1)%p
      FRAGMENTS(4)%p => FRAGMENTS(2)%p
      AObasis(3)%p => Setting%BASIS(1)%p
      AObasis(4)%p => Setting%BASIS(2)%p
      AOstring(3) = AO1
      AOstring(4) = AO2
      sameFrag(3,4) = sameFrag(1,2)
      sameFrag(4,3) = sameFrag(2,1)
      sameRHSaos = sameLHSaos
      batchindex(3) = SETTING%batchindex(1) 
      batchindex(4) = SETTING%batchindex(2) 
      IF(batchindex(3).NE.0.OR.batchindex(4).NE.0)THEN
         batchsize(1) = SETTING%batchsize(1) 
         batchsize(2) = SETTING%batchsize(2) 
         batchsize(3) = SETTING%batchsize(1) 
         batchsize(4) = SETTING%batchsize(2) 
      ENDIF
   ELSE
      FRAGMENTS(1)%p => FRAGMENTS(3)%p
      FRAGMENTS(2)%p => FRAGMENTS(4)%p
      AObasis(1)%p => Setting%BASIS(3)%p
      AObasis(2)%p => Setting%BASIS(4)%p
      AOstring(1) = AO3
      AOstring(2) = AO4
      sameFrag(1,2) = sameFrag(3,4)
      sameFrag(2,1) = sameFrag(4,3)
      sameLHSaos = sameRHSaos
      batchindex(1) = SETTING%batchindex(3) 
      batchindex(2) = SETTING%batchindex(4) 
      IF(batchindex(1).NE.0.OR.batchindex(2).NE.0)THEN
         batchsize(1) = SETTING%batchsize(3) 
         batchsize(2) = SETTING%batchsize(4) 
         batchsize(3) = SETTING%batchsize(3) 
         batchsize(4) = SETTING%batchsize(4) 
      ENDIF
   ENDIF
   sameFrag(1,3) = .TRUE.
   sameFrag(2,4) = .TRUE.
   sameFrag(3,1) = .TRUE.
   sameFrag(4,2) = .TRUE.
   IF(sameFrag(1,2))then
      sameFrag(1,4) = .TRUE.
      sameFrag(2,3) = .TRUE.
      sameFrag(3,2) = .TRUE.
      sameFrag(4,1) = .TRUE.
   ELSE
      sameFrag(1,4) = .FALSE.
      sameFrag(2,3) = .FALSE.
      sameFrag(3,2) = .FALSE.
      sameFrag(4,1) = .FALSE.
   ENDIF
   sameODs = .TRUE.
ENDIF

DO iAO=1,setting%nAO
  if(batchindex(iAO).NE. 0)THEN
     FRAGMENTS(iAO)%p => SETTING%MOLECULE(iAO)%p
  endif
ENDDO
!ENDIF
   
IF (intType.EQ.Primitiveinttype) THEN
  intnrm = .TRUE.
ELSEIF (intType.EQ.Contractedinttype) THEN
  intnrm = .FALSE.
ELSE
  WRITE(LUPRI,'(1X,2A)') 'Wrong case in BuildInputAO, intType =',intType
  CALL LSQUIT('Error - wrong intType in BuildInputAO',lupri)
ENDIF

nAObuilds = 0
DO IAO=1,4
   uniqueAO = .true.
   DO JAO=1,IAO-1
      IF ((AOstring(IAO).EQ.AOstring(JAO)).AND.sameFrag(iAO,jAO)) THEN
         uniqueAO = .false.
         ind = indexUnique(JAO)
        EXIT
     ENDIF
  ENDDO
  IF (SETTING%SCHEME%FRAGMENT) uniqueAO = .true.
  IF (uniqueAO) THEN
    nAObuilds  = nAObuilds+1
    ind        = nAObuilds
    indexUnique(IAO) = ind
    CALL SetAObatch(AObuild(ind),batchindex(iAO),batchsize(iAO),ndim(ind),AOstring(iAO),intType,&
     &              SETTING%Scheme,FRAGMENTS(iAO)%p,AObasis(iAO)%p,LUPRI,LUERR)
    IF (AOstring(IAO).EQ.AOpCharge) THEN
      sameLHSaos = .FALSE.
      sameRHSaos = .FALSE.
    ENDIF
  ENDIF
  indAO(iAO) = ind
ENDDO

DEALLOCATE(FRAGMENTS)

END SUBROUTINE BuildInputAO

!> \brief Sets up the AO-batch
!> \author S. Reine
!> \date 18-03-2010
!> \param AObatch The AO-batch
!> \param AO Specifying what basis set to use: 'Regular', 'DF-Aux', ...
!> \param intType Specifying contracted or primitive basis
!> \param AOtype Specifying basis-function type: 'Hermite', 'Cartesian', 'Default'
!> \param Scheme Specifies integral scheme
!> \param Molecule The molecule
!> \param Basis The basis set
!> \param LUPRI Default output unit
!> \param LUERR Deafult error unit
SUBROUTINE SetAObatch(AObatch,batchindex,batchsize,nDim,AO,intType,Scheme,Molecule,Basis,LUPRI,LUERR)
implicit none
TYPE(AOITEM),intent(INOUT)        :: AObatch
Integer,intent(IN)                :: batchindex
Integer,intent(IN)                :: batchsize
Integer,intent(OUT)               :: nDim
integer,intent(IN)                :: AO,intType
Type(LSINTSCHEME),intent(IN)      :: Scheme
Type(MOLECULEINFO),pointer        :: Molecule
Type(BASISINFO),pointer           :: Basis
Integer,intent(IN)                :: LUPRI,LUERR
!
TYPE(BASISSETINFO),pointer :: AObasis,AObasis2
Logical :: uncont,intnrm,emptyAO,AddBasis2
integer :: AObatchdim,iATOM,nAObatches,batchindex2
Character(len=8)     :: AOstring
uncont = scheme%uncont
IF (intType.EQ.Primitiveinttype) THEN
  intnrm = .TRUE.
ELSEIF (intType.EQ.Contractedinttype) THEN
  intnrm = .FALSE.
ELSE
  WRITE(LUPRI,'(1X,2A)') 'Wrong case in SetAObatch, intType =',intType
  CALL LSQUIT('Error - wrong intType in SetAObatch',lupri)
ENDIF

AddBasis2 = .FALSE.
emptyAO = .false.
SELECT CASE(AO)
CASE (AORegular)
   AObasis => Basis%BINFO(RegBasParam)  !Regular Basis
CASE (AOdfAux)
   AObasis => Basis%BINFO(AUXBasParam)  !AUXILIARY Basis
CASE (AOdfCABS)
   AObasis => Basis%BINFO(RegBasParam)  !Regular Basis
   AObasis2 => Basis%BINFO(CABBasParam)  !CABS Basis
   AddBasis2 = .TRUE.
CASE (AOdfCABO)
   AObasis => Basis%BINFO(CABBasParam)  !CABS Basis only 
CASE (AOdfJK)
   AObasis => Basis%BINFO(JKBasParam)   !JK Basis
CASE (AOVAL)
   AObasis => Basis%BINFO(VALBasParam)  !VALENCE Basis
CASE (AOadmm)
   AObasis => Basis%BINFO(ADMBasParam)  !ADMM Basis
CASE (AOEmpty)
   emptyAO = .true.
   CALL BUILD_EMPTY_AO(AObatch,LUPRI)
   nDim = 1
CASE (AONuclear)
   emptyAO = .true.
   CALL BUILD_EMPTY_NUCLEAR_AO(AObatch,Molecule,LUPRI)
   nDim = 1
CASE (AONuclearSpec)
   !specific nuclei 
   emptyAO = .true.
   IATOM = scheme%AONuclearSpecID 
   CALL BUILD_EMPTY_SINGLE_NUCLEAR_AO(AObatch,Molecule,LUPRI,IATOM)
   nDim = 1
CASE (AOpCharge)
   emptyAO = .true.
   CALL BUILD_EMPTY_PCHARGE_AO(AObatch,Molecule,LUPRI)
   nDim = Molecule%nAtoms
CASE (AOelField)
   emptyAO = .true.
   CALL BUILD_EMPTY_ELFIELD_AO(AObatch,Molecule,LUPRI)
   nDim = 3
CASE DEFAULT
   print*,'Programming error: Not a case in SetAObatch: case: ',AO
   WRITE(lupri,*) 'Programming error: Not a case in SetAObatch: case: ',AO
   call param_AO_Stringfromparam(AOstring,AO)
   WRITE(luerr,*) 'Programming error: Not a case in SetAObatch: case: ',AOstring
   CALL LSQuit('Programming error: Not a case in SetAObatch!',lupri)
END SELECT
IF (.not.emptyAO) THEN
   IF(batchindex.EQ. 0)THEN
      CALL BUILD_AO(LUPRI,SCHEME,SCHEME%AOPRINT,&
           &              Molecule,AObasis,AObatch,&
           &              uncont,intnrm,.FALSE.)
      IF(AddBasis2)THEN
         CALL BUILD_AO(LUPRI,SCHEME,SCHEME%AOPRINT,&
              &              Molecule,AObasis2,AObatch,&
              &              uncont,intnrm,AddBasis2)
      ENDIF
      nDim = getNbasis(AO,intType,Molecule,LUPRI)
   ELSE
      IF(AddBasis2)THEN
         Call determinenAObatches(nAObatches,LUPRI,SCHEME,&
              & SCHEME%AOPRINT,molecule,AObasis,uncont,intnrm)
         !print*,'batchindex',batchindex,'nAObatches',nAObatches
         IF(batchindex.LE.nAObatches)THEN
            CALL BUILD_SHELLBATCH_AO(LUPRI,SCHEME,&
                 & SCHEME%AOPRINT,molecule,AObasis,AObatch,&
                 & uncont,intnrm,batchindex,AObatchdim,batchsize)
         ELSE
            batchindex2=batchindex-nAObatches
            !print*,'batchindex2',batchindex2,'nAObatches',nAObatches
            CALL BUILD_SHELLBATCH_AO(LUPRI,SCHEME,&
                 & SCHEME%AOPRINT,molecule,AObasis2,AObatch,&
                 & uncont,intnrm,batchindex2,AObatchdim,batchsize)
         ENDIF
      ELSE
         CALL BUILD_SHELLBATCH_AO(LUPRI,SCHEME,&
              & SCHEME%AOPRINT,molecule,AObasis,AObatch,&
              & uncont,intnrm,batchindex,AObatchdim,batchsize)
      ENDIF
      nDim = AObatchdim
   ENDIF
ENDIF

END SUBROUTINE SetAObatch

!> \brief frees the input AOs
!> \author T. Kjaergaard and S. Reine
!> \date 2010
!> \param AObuild the list of AOITEMS to be build
!> \param nAObuilds the number of AOITEMS build
!> \param LUPRI logical unit number of the Default output file
SUBROUTINE FreeInputAO(AObuild,nAObuilds,LUPRI)
implicit none
Integer              :: LUPRI,nAObuilds
TYPE(AOITEM),target  :: AObuild(4)
!
integer :: iAO

DO iAO=1,nAObuilds
  CALL free_aoitem(lupri,AObuild(iAO))
ENDDO
 
END SUBROUTINE FreeInputAO
END MODULE ls_inputAO_mod

