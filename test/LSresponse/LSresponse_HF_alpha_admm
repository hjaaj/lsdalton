#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSresponse_HF_alpha_admm.info <<'%EOF%'
   LSresponse_HF_alpha_admm
   -------------------
   Molecule:         H2O2
   Wave Function:    HF / 6-31+G* / 3-21G (JK aux.basis for ADMM)
   Test Purpose:     Test real and complex polarizability in LSDALTON
                     using the ADMM approximation for the exchange contribution
                     (Patrick Merlot).
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSresponse_HF_alpha_admm.mol <<'%EOF%'
BASIS
6-31G* Aux=3-21G ADMM=3-21G
H2O

Atomtypes=2 Angstrom Nosymmetry
Charge=8.0 Atoms=1
O     -0.00000048765313      0.40820236609260      0.00000000000000
Charge=1.0 Atoms=2
H      0.75888506250693     -0.20410082647718      0.00000000000000
H     -0.75888457485381     -0.20410153961542      0.00000000000000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSresponse_HF_alpha_admm.dal <<'%EOF%'
**INTEGRALS
.ADMM
**WAVE FUNCTIONS
.HF
*DENSOPT
.NVEC
8
.CONVTHR
1.0D-6
**RESPONS
*SOLVER
.CONVTHR
1.0D-6
*ALPHA
.BFREQ
2
0.1  0.20007543
.IMBFREQ
2
0.0  0.001
*END OF INPUT  
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL > LSresponse_HF_alpha_admm.check
cat >> LSresponse_HF_alpha_admm.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * HF energy\: * \-76\.01212" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"

# Memory test
CRIT2=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT2`
CTRL[2]=1
ERROR[2]="Memory leak -"

# Real Polarizability tests
CRIT1=`$GREP "Ex *  5.759[0-9][0-9][0-9][0-9] " $log | wc -l`
CRIT2=`$GREP "Ey .*  2.3987[0-9][0-9][0-9]"     $log | wc -l`
CRIT3=`$GREP "Ez .*  2.542[0-9][0-9][0-9]"      $log | wc -l`

TEST[3]=`expr  $CRIT1 \+ $CRIT2 \+ $CRIT3 `
CTRL[3]=3
ERROR[3]="Error in real polarizability"

# Imaginary part of complex polarizability tests
CRIT1=`$GREP "Ex * 0.6324[0-9][0-9][0-9][0-9]E-01"  $log | wc -l`
CRIT2=`$GREP "Ey .* 0.1105[0-9][0-9][0-9][0-9]E-02" $log | wc -l`
CRIT3=`$GREP "Ez .* 0.1429[0-9][0-9][0-9][0-9]E-02" $log | wc -l`

TEST[4]=`expr  $CRIT1 \+ $CRIT2 \+ $CRIT3 `
CTRL[4]=3
ERROR[4]="Error in imaginary part of complex polarizability"


PASSED=1
for i in 1 2 3 4
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
