# Chimera python script to plot orbitals from LSDalton
# This is useful to set default values for contour and colors
# Just modify the filenames below and execute as,
#   $ chimera plt_orbs.py
#
# Author: Pablo Baudin
# Date: November 2016

from chimera import runCommand

xyz = "/home/pablo/biva.xyz"
occ = "/home/pablo/nto_occ156_exc001.plt"
vir = "/home/pablo/nto_vir580_exc001.plt"

threshold = 0.02 # threshold for plots
# (red, green, blue, opacity) color definition
blue = (.2, .2, 1.0, 0.8)
lblue = (.7, .7, 1.0, 0.8)
red = (1.0, .2, .2, 0.8)
lred = (1.0, .7, .7, 0.8)

def set_volume_threshold_and_color(threshold, rgb1, rgb2):
    import VolumeViewer as vv
    v = vv.active_volume()
    v.set_parameters(surface_levels = [threshold,-threshold],      # Set threshold level.
                     surface_colors = [rgb1,rgb2])  # Set color rgb or rgba.
    v.show()

# open xyz file
runCommand('open %s' % xyz)

# open and plot occ orbs
runCommand('open %s' % occ)
set_volume_threshold_and_color(threshold, red, lred)

# open vir orbs
runCommand('open %s' % vir)
set_volume_threshold_and_color(threshold, blue, lblue)


